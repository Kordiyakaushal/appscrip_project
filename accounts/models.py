from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
# from phone_field import PhoneField
from django.core.validators import RegexValidator
from rest_framework.reverse import reverse as api_reverse

class CustomUserManager(BaseUserManager):
    def create_user(self,email,name=None,contact_no=None,city=None,address=None,password=None,is_active=True,is_staff=False,is_admin=False):
        if not email:
            raise ValueError('user must have email')
        if not password:
            raise ValueError("user must have password")
        user_obj = self.model(
            email=self.normalize_email(email),
            name = name,
            contact_no = contact_no,
            city = city,
            address = address
        )
        user_obj.set_password(password)
        user_obj.active = is_active
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.save(using=self._db)
        return user_obj

    def create_superuser(self,email,name=None,contact_no=None,city=None,address=None,password=None):
        user = self.create_user(
            email,
            name = name,
            contact_no = contact_no,
            city = city,
            address = address,
            password=password,
            is_staff=True,
            is_admin = True
        )
        return user
    
    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id) # Product.objects == self.get_queryset()
        if qs.count() == 1:
            return qs.first()
        return None


class CustomUser(AbstractBaseUser):
    email = models.EmailField(max_length=255,unique=True,default='abc@gmail.com')
    name = models.CharField(max_length=255,blank=True,null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    contact_no = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    city = models.CharField(max_length=255,blank=True,null=True)
    address = models.CharField(max_length=255,blank=True,null=True)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name','contact_no','city','address',]
    
    objects = CustomUserManager()
    
    def __str__(self):
        return self.email

    def has_perm(self,perm,obj=None):
        return True

    def has_module_perms(self,app_label):
        return True

    def get_api_url(self, request=None):
        return api_reverse("update", kwargs={'pk': self.pk}, request=request)
    @property
    def is_active(self):
        return self.active

    @property
    def is_staff(self):
        return self.staff
   
    @property
    def is_admin(self):
        return self.admin