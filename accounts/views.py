from django.contrib.auth import authenticate, login, get_user_model
from django.views.generic import CreateView, FormView, ListView, UpdateView, DeleteView
from django.shortcuts import render,redirect
from .models import CustomUser
from .forms import LoginForm, RegisterForm
from django.urls import reverse_lazy
from django.contrib.auth.mixins import UserPassesTestMixin

class LoginView(FormView):
    form_class = LoginForm
    success_url = '/'
    template_name = 'accounts/login.html'

    def form_valid(self, form):
        request = self.request
        email  = form.cleaned_data.get("email")
        password  = form.cleaned_data.get("password")
        user = authenticate(request, email=email, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        return super(LoginView, self).form_invalid(form)

class RegisterView(UserPassesTestMixin,CreateView):
    def test_func(self):
        return self.request.user.admin
    form_class = RegisterForm
    template_name = 'accounts/register.html'
    success_url = '/'


class homePage(ListView):
    queryset = CustomUser.objects.all()
    template_name = 'accounts/index.html'
    model = CustomUser

    def get_context_data(self,*args,**kwargs):
        context = super(homePage,self).get_context_data(*args,**kwargs)
        return context

class UpdatePage(UserPassesTestMixin,UpdateView):
    def test_func(self):
        return self.request.user.admin

    model = CustomUser
    fields = ('name','contact_no','city','address',)
    success_url = '/'
    template_name = 'accounts/update.html'

    def get_object(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        instance = CustomUser.objects.get_by_id(pk)
        if instance is None:
            raise Http404("User doesn't exist")
        return instance

class UserDelete(UserPassesTestMixin,DeleteView):
    def test_func(self):
        return self.request.user.admin
    model = CustomUser
    success_url = '/'
    template_name = 'accounts/delete.html'