from django.test import TestCase
from django.contrib.auth import get_user_model
User = get_user_model()
from accounts.forms import RegisterForm,LoginForm,UpdateForm
from django.urls import reverse

class LogInTest(TestCase):
    def setUp(self):
        test_user1 = User.objects.create_user(email='test1@gmail.com', password='1X<ISRUkw+tuK',contact_no='7069906622')
        test_user2 = User.objects.create_user(email='test2@gmail.com', password='2HJ1vRV0Z&3iD',contact_no='7069906623')

        test_user1.save()
        test_user2.save()

    def test_login(self):
        login = self.client.login(email='test2@gmail.com', password='2HJ1vRV0Z&3iD')
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
       

    def test_register_form_email_label(self):
        form = RegisterForm()
        self.assertTrue(form.fields['email'].label == 'Email')

    def test_register_form_email_label(self):
        form = LoginForm()
        self.assertTrue(form.fields['email'].label == 'Email')
