from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from accounts.forms import RegisterForm,LoginForm,UpdateForm

User = get_user_model()


class RegisterViewTest(TestCase):
    def setUp(self):
        # Create two users
        test_user1 = User.objects.create_user(email='test1@gmail.com', password='1X<ISRUkw+tuK',contact_no='7069906622')
        test_user2 = User.objects.create_user(email='test2@gmail.com', password='2HJ1vRV0Z&3iD',contact_no='7069906623')
        
        test_user1.save()
        test_user2.save()
    
    def test_logged_in_with_permission(self):
        login = self.client.login(email='test2@gmail.com', password='2HJ1vRV0Z&3iD')
        response = self.client.get(reverse('home'))
        
        self.assertEqual(response.status_code, 200)

    def test_register_form_email_label(self):
        form = RegisterForm()
        self.assertTrue(form.fields['email'].label == 'Email')   
    
